var elem = document.getElementById( "sub" );

elem.addEventListener( "click", function( event ) {
        event.preventDefault();

        var mot = document.getElementById("mot").value;

		var xmlhttp = new XMLHttpRequest();

		xmlhttp.onreadystatechange = function(){

		    if( xmlhttp.readyState == 4 && xmlhttp.status == 200 ){

			  	var str = xmlhttp.responseText;
				var jsonparse = JSON.parse( xmlhttp.responseText );
				var final = "";
				var a = "";
				var div = "";

				for( var i = 0; i<jsonparse[1].length; i++ ){

					final = jsonparse[1][i];
					descr = jsonparse[2][i];
					a = jsonparse[3][i];
					div += "<p><a href=" + a + ">" + final + "</a> : " + descr + "</p>";

			  		document.getElementById( "myDiv" ).innerHTML = div;

				}
		    }
		}

		xmlhttp.open("GET","http://localhost:1234/api?action=opensearch&format=json&formatversion=2&search="+mot+"&namespace=0&limit=5&suggest=true",true);
		xmlhttp.send();
});